# Budget Rent A Car (Nicaragua) - Ionic mobile application for vehicles revisions

## System requirements

- Ionic 2.2.3
- Cordova 7.1.0
- Node 8.9.1
- Bower 1.8.0
- Gulp 3.9.1

To generate an APK:

- Java 8
- Gradle 4.3.1
- Android SDK 25

## Installation
Run bower and npm to get all dependencies needed.

```shell
$ npm install
$ bower install
```

Execute `gulp updateLib` to generate the `lib` folder with bower dependencies.

## Runing on Android device
In order to run the application on an Android device:

1. Add Android platform

```shell
$ ionic platform add android
```

2. Connect the device to the computer, allow USB debugging and then execute

```shell
$ ionic run android
```

## Setting up tests
In order to run Karma and protractor tests, please do the following:

Install Karma CLI globally

```shell
$ npm install -g karma-cli
```

Install Protractor globally

```shell
$ npm install -g protractor
```

Use the web manager to install the chrome-driver and selenium server.

```shell
$ webdriver-manager update
```

To run protractor tests, execute:

```shell
$ ionic serve
$ webdriver-manager start  
$ protractor protractor.conf.js
```

## Generate an APK

### Rebuild splash and icon images

As default, the images are in `resources/android/{icon.png, splash.png}`. Is preferred to rebuild in each APK release.

```shell
$ ionic resources --icon
$ ionic resources --splash
```

### If the Rebuild doesn't work try
1.	Go to config.xml
2.	Go to <platform name="android"> tag;
3.	Leave only the following tags

```
<icon density="ldpi" src="resources/android/icon/drawable-ldpi-icon.png"/>
<splash density="land-ldpi" src="resources/android/splash/drawable-land-ldpi-screen.png"/>
<splash density="port-ldpi" src="resources/android/splash/drawable-port-ldpi-screen.png"/>
```

4.	Go to resources/android folder and create "icon" and "splash" folder
5. Create a copy from icon.png, and move it to de new icon folder and rename it "drawable-ldpi-icon.png"
6. Create a copy from splash.png, and move it to de new splash folder and rename it "drawable-land-ldpi-screen.png"
7. Create a copy from splash.png, and move it to de new splash folder and rename it "drawable-port-ldpi-screen.png"

### Build the APK

```shell
$ ionic build android
```

### Add Android platform to build APK

```shell
$ ionic platform add android
```

## Styleguide

Refer to AngularJS John Papa Styleguide: https://github.com/johnpapa/angular-styleguide

### This project is running with a older version of ECMA Script
The project doesn't allow arrow functions, let variable and template strings