(function () {
  'use strict';

  // var PRODUCTION_PATH = 'https://budget-nic-ws.herokuapp.com';
  var STAGING_PATH = 'https://budget-nic-ws-staging.herokuapp.com';
  // var LOCAL_PATH = 'http://localhost:8080';

  angular
    .module('app')
    .constant('SIGNATURE_URL', STAGING_PATH + '/api/v1/revisions');
})();
