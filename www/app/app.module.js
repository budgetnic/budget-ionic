(function() {
  'use strict';

  angular
    .module('app', [
      'ionic',
      'ui.router',
      'firebase',
      'app.services',
      'app.login',
      'app.signature',
      'app.scanMenu',
      'app.carView',
      'app.content',
      'app.carDeliveryInfo',
      'app.tireRevision',
      'app.carInfo',
      'app.scanner',
      'app.services',
      'app.observations',
      'app.scannerError',
      'ngCordova',
      'app.carParts',
      'app.Movements'
    ]);
})();
