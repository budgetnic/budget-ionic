(function() {
  'use strict';

  angular
    .module('app')
    .run(run);

  /* @ngInject */
  function run($ionicPlatform) {
    //staging
    var stagingConfig = {
      apiKey: "AIzaSyD5RHeoUGIylWvnUnu4i6rFHA-Y_JHzL2g",
      authDomain: "budget-nic-developer.firebaseapp.com",
      databaseURL: "https://budget-nic-developer.firebaseio.com",
      projectId: "budget-nic-developer",
      storageBucket: "budget-nic-developer.appspot.com",
      messagingSenderId: "860073900558"
    };

    //prod
    var productionConfig = {
      apiKey: 'AIzaSyA1xp7UhFGmVRR7yHi36vR2ncGSR0Qrlno',
      authDomain: 'budget-nic.firebaseapp.com',
      databaseURL: 'https://budget-nic.firebaseio.com',
      storageBucket: 'budget-nic.appspot.com',
      messagingSenderId: '128066998170'
    };

    firebase.initializeApp(stagingConfig);

    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  }

})();
