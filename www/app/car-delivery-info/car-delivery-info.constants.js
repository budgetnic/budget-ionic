(function() {
  'use strict';
  angular
    .module('app.carDeliveryInfo')
    .constant('GAS_LEVELS', [
      { id: '0', name: 'Vacio' },
      { id: '1', name: '1/8' },
      { id: '2', name: '2/8' },
      { id: '3', name: '3/8' },
      { id: '4', name: '4/8' },
      { id: '5', name: '5/8' },
      { id: '6', name: '6/8' },
      { id: '7', name: '7/8' },
      { id: '8', name: 'Lleno' }
    ])
    .constant('GAS_LEVEL_SELECTED', { id: '0', name: 'Vacio' })
    .constant('PLACE_ABBREVIATIONS', [
      { abbreviation: 'AE', name: 'aeropuerto' },
      { abbreviation: 'HI', name: 'holiday_inn' },
      { abbreviation: 'CM', name: 'carretera_masaya km.12' },
      { abbreviation: 'MC', name: 'oficina_central' },
      { abbreviation: 'CH', name: 'chinandega' },
      { abbreviation: 'GR', name: 'granada' },
      { abbreviation: 'MAT', name: 'matagalpa' },
      { abbreviation: 'SJ', name: 'san_juan' },
      { abbreviation: 'FL', name: 'flota' },
      { abbreviation: 'TA', name: 'taller' },
      { abbreviation: 'OVF', name: 'oficina_villa fontana' },
    ])
    .constant('MOVEMENT_TYPE_ABBREVIATIONS', [
      { abbreviation: 'TR', name: 'CarMov' },
      { abbreviation: 'UNP', name: 'Other' },
      { abbreviation: 'AL', name: 'CarRent' },
      { abbreviation: 'ED', name: 'HomeDelivery' }
    ]);
})();