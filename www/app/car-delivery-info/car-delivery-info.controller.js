(function () {
  'use.strict';

  angular
    .module('app.carDeliveryInfo')
    .controller('CarDeliveryInfoCtrl', CarDeliveryInfoCtrl);

  /* @ngInject */
  function CarDeliveryInfoCtrl($state,
                              $stateParams,
                              CarDeliveryInfoService,
                              RevisionService,
                              LastRevisionService,
                              FirebaseRevisionService,
                              GAS_LEVELS,
                              GAS_LEVEL_SELECTED,
                              PLACE_ABBREVIATIONS,
                              MOVEMENT_TYPE_ABBREVIATIONS,
                              $ionicNavBarDelegate,
                              lodash) {

    var vm = this;
    vm.lastRevision = LastRevisionService.revision;
    vm.disabledEmail = ($stateParams.option === 'CarMov' || $stateParams.option === 'Other') ? "si":"no"
    vm.selectedOption = undefined;
    vm.km = getPreviousKm(vm.lastRevision);
    vm.goToTireRevision = goToTireRevision;
    vm.updateRevisionId = updateRevisionId;
    vm.gasLevels = GAS_LEVELS;
    vm.deliveryPlaces = {};
    vm.deliveryInfo = {};
    vm.deliveryInfo.gasLevelSelected = vm.lastRevision ? getGasLevel(LastRevisionService) : GAS_LEVEL_SELECTED;
    vm.placeAbbreviations = PLACE_ABBREVIATIONS;
    FirebaseRevisionService.getDBRevisionCounter().then(function (counterObject) {
      if (vm.lastRevision && vm.lastRevision.type === 'check-in') {
        updateMovementTypeCounter(counterObject);
      } else if (!vm.lastRevision) {
        updateMovementTypeCounter(counterObject);
      }
    });

    activate();
    activateDeliveryOrigin();
    activateDeliveryDestiny();

    function updateMovementTypeCounter(counterObject) {
      if (vm.selectedOption === 'CarMov' && counterObject.transferCounter) {
        vm.revCounter = counterObject.transferCounter + 1;
        FirebaseRevisionService.setRevCounter(vm.revCounter);
      } else if (vm.selectedOption === 'CarMov') {
        vm.revCounter = 1;
        FirebaseRevisionService.setRevCounter(vm.revCounter);
      }

      if (vm.selectedOption === 'Other' && counterObject.noneProductiveCounter) {
        vm.revCounter = counterObject.noneProductiveCounter + 1;
        FirebaseRevisionService.setRevCounter(vm.revCounter);
      } else if (vm.selectedOption === 'Other') {
        vm.revCounter = 1;
        FirebaseRevisionService.setRevCounter(vm.revCounter);
      }
    }

    function getGasLevel(lastRevision) {
      return lodash.find(GAS_LEVELS, function (level) { return level.name === lastRevision.revision.gas_level })
    }

    function activate() {
      vm.selectedOption = $stateParams.option;
      $ionicNavBarDelegate.showBackButton(true);
      CarDeliveryInfoService.initDeliveryPlaces()
        .then(function () {
          vm.deliveryPlaces = lodash.filter(CarDeliveryInfoService.deliveryPlaces, function (filterPlace) {return filterPlace.name !== 'Esteli' });
        })
    }



    function getDeliveryPlace(deliveryType) {
      const lastRevision = LastRevisionService.revision ? LastRevisionService.revision[deliveryType] : null;
      return lastRevision ? lodash.find(CarDeliveryInfoService.deliveryPlaces, function (revision) { return revision.name === lastRevision }) : CarDeliveryInfoService.deliveryPlaces[0];
    }

    function activateDeliveryOrigin() {
      vm.selectedOption = $stateParams.option;
      $ionicNavBarDelegate.showBackButton(true);
      CarDeliveryInfoService.initDeliveryPlaces()
        .then(function () {
          vm.deliveryPlaces = CarDeliveryInfoService.deliveryPlaces;
        });
    }

    function updateRevisionId() {
      if (vm.selectedOption === "CarMov" || vm.selectedOption === 'Other') {
        const movementTypeAbbreviation = getMovementTypeAbbreviation(vm.selectedOption);
        if (vm.lastRevision) {
          vm.revisionId = (isCarMoveOrNonProductiveMovementType() && vm.lastRevision.type === 'check-in') ? movementTypeAbbreviation + "-" + getPlaceAbbreviation(vm.deliveryPlacesOrigin.name) + "-" + vm.revCounter : vm.lastRevision.numberMovement;
        }
        else {
          if (isCarMoveOrNonProductiveMovementType())
            vm.revisionId = movementTypeAbbreviation + "-" + getPlaceAbbreviation(vm.deliveryPlacesOrigin.name) + "-" + vm.revCounter
        }
        RevisionService.setRevId(vm.revisionId);
      }else{
        vm.deliveryPlacesOrigin = vm.deliveryPlace;
      }
    }

    function isCarMoveOrNonProductiveMovementType() {
      return (vm.selectedOption === "CarMov" || vm.selectedOption === "Other");
    }
    function getPlaceAbbreviation(deliveryPlaceName) {
      for (var j = 0; j < PLACE_ABBREVIATIONS.length; j++) {
        if (PLACE_ABBREVIATIONS[j].name === deliveryPlaceName.toLowerCase().replace(' ', '_')) {
          return PLACE_ABBREVIATIONS[j].abbreviation;
        }
      }
    }

    function getMovementTypeAbbreviation(selectedOption) {
      return lodash.find(MOVEMENT_TYPE_ABBREVIATIONS, function (type) {
        return type.name === selectedOption
      }).abbreviation
    }



    function activateDeliveryDestiny() {
      vm.selectedOption = $stateParams.option;
      $ionicNavBarDelegate.showBackButton(true);
      CarDeliveryInfoService.initDeliveryPlaces()
        .then(function () {
          vm.deliveryPlaces = CarDeliveryInfoService.deliveryPlaces;
        });
    }

    function resetFields() {
      vm.deliveryInfo = {
        deliveryPlaceSelected: CarDeliveryInfoService.deliveryPlaces[0],
        gasLevelSelected: GAS_LEVEL_SELECTED
      };
      vm.km = 0;
    }

    function createDeliveryInfoObject(km, deliveryPlaceRef, deliveryPlace, PlaceDestinyRef, deliveryPlaceDestiny, PlaceOriginRef, deliveryPlacesOrigin, gasLevel) {
      return {
        'km': km,
        'delivery_place_ref': deliveryPlaceRef,
        'delivery_place': deliveryPlace,
        'delivery_place_oref': PlaceOriginRef,
        'delivery_place_o': deliveryPlacesOrigin,
        'delivery_place_dref': PlaceDestinyRef,
        'delivery_place_d': deliveryPlaceDestiny,
        'gas_level': gasLevel
      };
    }

    function getPreviousKm(lastRevision) {
      return (lastRevision) ? lastRevision.km : 0;
    }

    function goToTireRevision() {
      var deliveryPlaceTemp;
      var deliveryPlaceDestinyTemp;
      var deliveryPlaceOriginTemp;
  
      var optiontype = vm.selectedOption;

      if(vm.deliveryPlace){
        CarDeliveryInfoService.deliveryPlaceSelected = vm.deliveryPlace;
        deliveryPlaceTemp = vm.deliveryPlace;
        deliveryPlaceDestinyTemp = vm.deliveryPlace;
        deliveryPlaceOriginTemp = vm.deliveryPlace;
      }else{
        CarDeliveryInfoService.deliveryPlaceSelected = vm.deliveryPlaceDestiny;
        CarDeliveryInfoService.deliveryPlaceSelected = vm.deliveryPlacesOrigin;
        deliveryPlaceTemp = vm.deliveryPlacesOrigin;
        deliveryPlaceDestinyTemp = vm.deliveryPlaceDestiny;
        deliveryPlaceOriginTemp = vm.deliveryPlacesOrigin;
      }

      var deliveryInfo = createDeliveryInfoObject(vm.km,
        deliveryPlaceTemp.$id,
        deliveryPlaceTemp.name,
        deliveryPlaceDestinyTemp.$id,
        deliveryPlaceDestinyTemp.name,
        deliveryPlaceOriginTemp.$id,
        deliveryPlaceOriginTemp.name,
        vm.deliveryInfo.gasLevelSelected.name);
      RevisionService.setCarDeliveryInfo(deliveryInfo, optiontype);
      resetFields();
      $state.go('tireRevision', { option: vm.selectedOption });
    }
  }
})();
