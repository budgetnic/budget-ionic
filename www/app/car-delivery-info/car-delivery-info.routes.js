(function() {
  'use strict';

  angular
    .module('app.carDeliveryInfo')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('carDeliveryInfo', {
        url: '/car_delivery_info',
        templateUrl: 'app/car-delivery-info/car-delivery-info.html',
        controller: 'CarDeliveryInfoCtrl',
        controllerAs: 'vm',
        params: { option: undefined },
        cache: false
      });
  }
})();
