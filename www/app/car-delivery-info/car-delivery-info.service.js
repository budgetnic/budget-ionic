(function() {
  'use strict';

  angular
    .module('app.carDeliveryInfo')
    .service('CarDeliveryInfoService', CarDeliveryInfoService);

  /* @ngInject */
  function CarDeliveryInfoService($q, $firebaseArray, $firebaseObject) {

    var rootRef = firebase.database().ref();
    var service = {
      initDeliveryPlaces: initDeliveryPlaces,
      deliveryPlaces: {},
      deliveryPlaceSelected: {}
    };

    return service;

    function getDeliveryPlaces() {
      var reference = rootRef
        .child('delivery_places');
      var deliveryPlaces = $firebaseArray(reference);
      return deliveryPlaces.$loaded();
    }

    function initDeliveryPlaces() {
      var deferred = $q.defer();
      getDeliveryPlaces()
        .then(function(deliveryPlaces) {
          service.deliveryPlaces = deliveryPlaces;
          deferred.resolve();
        });
      return deferred.promise;
    }
  }
})();
