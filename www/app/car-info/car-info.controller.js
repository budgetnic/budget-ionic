(function () {
  'use strict';

  angular
    .module('app.carInfo')
    .controller('CarInfoCtrl', CarInfoCtrl);

  /* @ngInject */
  function CarInfoCtrl(CarInfoFirebaseService,
    $state,
    $stateParams,
    RevisionService,
    LastRevisionService,
    $ionicNavBarDelegate) {

    var vm = this;
    vm.selectedOption = undefined;
    vm.goToCarView = goToCarView;
    vm.RevisionService = RevisionService;
    vm.CarInfoFirebaseService = CarInfoFirebaseService;

    activate();

    function setContractNumber(lastRevision) {
      if (lastRevision.contract_number) {
        vm.hasPreviousRevision = true;
        RevisionService.setContractNumber(lastRevision.contract_number);
        vm.revision = {};
        vm.revision.contract_number = lastRevision.contract_number;
      } else if (lastRevision.vehicle_ref === vm.CarInfoFirebaseService.carInfo.MVA) {
        vm.drive = lastRevision.drive;
        vm.phone = lastRevision.phone;
        vm.numberMovement = lastRevision.numberMovement;
      }
    }

    function setNewRevisionType() {
      var lastRevision = LastRevisionService.revision;
      if (lastRevision) {
        RevisionService.setNewType(lastRevision.type);
        if (angular.equals(RevisionService.getRevision().type, 'check-in')) {
          setContractNumber(lastRevision);
        }
      } else {
        RevisionService.setNewType('check-in');
      }
    }

    function goToCarView() {
      RevisionService.setLicensePlate(CarInfoFirebaseService.carInfo.license_plate);
      RevisionService.setCarMVA(CarInfoFirebaseService.carInfo.MVA);
      RevisionService.setOptions(vm.selectedOption);
      if (vm.selectedOption == 'CarMov' || vm.selectedOption == 'Other') {
        RevisionService.setInfoDriver(vm.drive, vm.phone, vm.numberMovement);
      }
      $state.go('carDeliveryInfo', { option: vm.selectedOption });
    }

    function activate() {
      vm.selectedOption = $stateParams.option;
      vm.hasPreviousRevision = false;
      $ionicNavBarDelegate.showBackButton(true);
      setNewRevisionType();
      vm.isLoaded = (CarInfoFirebaseService.carInfo.model) ? true : false;
      setTimeout(function () {
        if (!vm.isLoaded) {
          $state.go('scanner-error');
        }
      }, 7000);
    }
  }
})();
