(function() {
  'use strict';

  angular
    .module('app.carInfo')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('carInfo', {
        url: '/car_info',
        templateUrl: 'app/car-info/car-info.html',
        controller: 'CarInfoCtrl',
        controllerAs: 'vm',
        resolve: {
          'CarInfoService': function(CarInfoFirebaseService, LastRevisionService) {
            return CarInfoFirebaseService.fetchCarInfo()
              .then(function() {
                return LastRevisionService.fetchRevisionData();
              });
          }
        },
        params: { option: undefined },
        cache: false
      });
  }
})();
