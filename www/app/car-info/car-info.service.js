(function() {
  'use strict';

  angular
    .module('app.carInfo')
    .service('CarInfoFirebaseService', CarInfoFirebaseService);

  /* @ngInject */
  function CarInfoFirebaseService($firebaseObject,
                                  ScannerService) {

    var rootRef = firebase.database().ref();
    var service = {
      fetchCarInfo: fetchCarInfo,
      carInfo: {},
      currentCarId: ScannerService.getCode(),
      currentRevisionId: null
    };

    return service;

    function fetchCarInfo() {
      var reference = rootRef
        .child('vehicles')
        .child(ScannerService.getCode());
      service.carInfo = $firebaseObject(reference);
      return service.carInfo.$loaded();
    }

  }

})();
