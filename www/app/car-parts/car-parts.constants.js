(function() {
  'use strict';
  angular
    .module('app.carParts')
    .constant('ACCESORIES', [
      {'name': 'Antena' ,  'key': 'antenna'},
      {'name': 'Llanta de repuesto' , 'key': 'spare_tire'},
      {'name': 'Radio' , 'key': 'radio'},
      {'name': 'Cámara' , 'key': 'camera'},
      {'name': 'Herramientas' , 'key': 'tools'},
      {'name': 'Triángulo' , 'key': 'triangle'},
      {'name': 'Extinguidor' , 'key': 'extinguisher'},
      {'name': 'Rodamiento del año' , 'key': 'bearing_year'},
      {'name': 'Circulación' , 'key': 'circulation'},
      {'name': 'Seguro' , 'key': 'insurance'},
      {'name': 'Emisión de Gases' , 'key': 'emission_gases'},
      {'name': 'Inspección Mecanica' , 'key': 'mechanical_inspection'},
      {'name': 'Rack', 'key': 'rack'},
      {'name': 'Alfombras' , 'key': 'carpet'},
      {'name': 'Loderas' , 'key': 'mud_flap'},
      {'name': 'Llave' , 'key': 'keey'},
      {'name': 'Forros' , 'key': 'seat_covers'},
      {'name': 'Pide Vías' , 'key': 'side_lights'},
      {'name': 'Cañuelas' , 'key': 'roof_molding'},
      {'name': 'Cepillos Tricos' , 'key': 'windscreen'},
      {'name': 'Llavines' , 'key': 'doorlock'},
      {'name': 'Retrovisor Interno' , 'key': 'rear_view_mirror'},
      {'name': 'Tapón de Combustible' , 'key': 'fuel_cap'},
      {'name': 'Tapones de Motor' , 'key': 'engine_caps'}
    ])
    .constant('COUNTED_ACCESORIES', [
      {'name': 'Emblemas', 'key': 'emblems'},
      {'name': 'Placas', 'key': 'plates'}
    ])
    .constant('SELECTED_ACCESORIES', {
      'antenna': 'true',
      'spare_tire': 'true',
      'radio': 'true',
      'camera': 'true',
      'triangle': 'true',
      'extinguisher': 'true',
      'bearing_year': 'true',
      'circulation': 'true',
      'insurance': 'true',
      'emission_gases': 'true',
      'mechanical_inspection': 'true',
      'emblems': 'true',
      'tools': 'true',
      'plates': 'true',
      'rack': 'true',
      'carpet': 'true',
      'mud_flap': 'true',
      'keey': 'true',
      'seat_covers': 'true',
      'side_lights': 'true',
      'roof_molding': 'true',
      'windscreen': 'true',
      'doorlock': 'true',
      'rear_view_mirror': 'true',
      'fuel_cap': 'true',
      'engine_caps': 'true'

    })
    .constant('SELECTED_COUNTED_ACCESORIES', {
      emblems: '2',
      plates: '2'
    })
    .constant('ACCESSORY_SELECT_OPTIONS', {
      na: {value: 'na', text: 'N/A' },
      no: {value: 'false',text: 'No' },
      yes: {value: 'true',text: 'Sí'},
    });
})();
