(function() {
  'use strict';

  angular
    .module('app.carParts')
    .controller('CarPartsCtrl', CarPartsCtrl);

  /* @ngInject */
  function CarPartsCtrl($state,
                        $stateParams, 
                        ACCESORIES,
                        COUNTED_ACCESORIES,
                        SELECTED_ACCESORIES,
                        SELECTED_COUNTED_ACCESORIES,
                        ACCESSORY_SELECT_OPTIONS,
                        RevisionService,
                        ObservationsService,
                        LastRevisionService) {

    var vm = this;
    vm.selectedOption = undefined;
    vm.goToEndOrFeedback = goToEndOrFeedback;

    activate();

    function setValuesAndOptions() {
      vm.parts = ACCESORIES;
      vm.countedParts = COUNTED_ACCESORIES;
      vm.accesory = LastRevisionService.revision ? LastRevisionService.revision.car_parts_present : SELECTED_ACCESORIES;
      vm.countedAccesory = SELECTED_COUNTED_ACCESORIES;
      vm.accessorySelectOptions = ACCESSORY_SELECT_OPTIONS;
    }

    function activate() {
      vm.selectedOption = $stateParams.option;
      setValuesAndOptions();
    }

    function goToEndOrFeedback() {
      RevisionService.setObservations(ObservationsService.getObservations());
      RevisionService.setCarAccesories(vm.accesory, vm.countedAccesory);
      $state.go('signature', { option: vm.selectedOption });
    }
  }
})();
