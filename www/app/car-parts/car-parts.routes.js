(function() {
  'use strict';

  angular
    .module('app.carParts')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('carParts', {
        url: '/car_parts',
        templateUrl: 'app/car-parts/car-parts.html',
        controller: 'CarPartsCtrl',
        controllerAs: 'vm',
        params: { option: undefined },
        cache: false
      });
  }
})();
