(function() {
'use strict';

angular
    .module('app.carView')
    .constant('DAMAGE_OPTIONS', [
      {'id': '0', 'name': 'Golpe'},
      {'id': '1', 'name': 'Rayon'},
      {'id': '2', 'name': 'Camanance'},
      {'id': '3', 'name': 'Raspon'},
      {'id': '4', 'name': 'Cascado'},
      {'id': '5', 'name': 'Chispeado'},
      {'id': '6', 'name': 'Quebrado'},
      {'id': '7', 'name': 'Reventado'}
      
    ])
    .constant('SEVERITY_OPTIONS', [
        {'id': '0', 'name': 'Leve'},
        {'id': '1', 'name': 'Fuerte'}
    ])
    .constant('COLOR_CANVAS','#ED5505')
    .constant('DAMAGE_TYPE_SELECTED', {'id': '0', 'name': 'Golpe'})
    .constant('SELECTED_PART', {'id': '0', 'name': 'Bumper Delantero'})
    .constant('SELECTED_SEVERITY', {'id': '0', 'name': 'Leve'})
    .constant('PARTS', [
      {'id': '0', 'name': 'Bumper Delantero'},
      {'id': '1', 'name': 'Bumper Trasero'},
      {'id': '2', 'name': 'Puerta Derecha Delantera'},
      {'id': '3', 'name': 'Puerta Derecha Trasera'},
      {'id': '4', 'name': 'Puerta Izquierda Trasera'},
      {'id': '5', 'name': 'Puerta Izquierda Delantera'},
      {'id': '6', 'name': 'Parabrisas'},
      {'id': '7', 'name': 'Ventana Trasera'},
      {'id': '8', 'name': 'Ventana Derecha Delantera'},
      {'id': '9', 'name': 'Ventana Derecha Trasera'},
      {'id': '10', 'name': 'Ventana Izquierda Trasera'},
      {'id': '11', 'name': 'Ventana Izquierda Delantera'},
      {'id': '12', 'name': 'Retrovisor Derecho'},
      {'id': '13', 'name': 'Retrovisor Izquierdo'},
      {'id': '14', 'name': 'Aro Delantero Izquierdo'},
      {'id': '15', 'name': 'Aro Trasero Izquierdo'},
      {'id': '16', 'name': 'Aro Delantero Derecho'},
      {'id': '17', 'name': 'Aro Trasero derecho'},
      {'id': '18', 'name': 'Faro Derecho Delantero'},
      {'id': '19', 'name': 'Faro Izquierdo Delantero'},
      {'id': '20', 'name': 'Faro Derecho Trasero'},
      {'id': '21', 'name': 'Faro Izquierdo Trasero'},
      {'id': '22', 'name': 'Guardabarros Delantero Izquierdo'},
      {'id': '23', 'name': 'Guardabarros Trasero Izquierdo'},
      {'id': '24', 'name': 'Guardabarros Delantero Derecho'},
      {'id': '25', 'name': 'Guardabarros Trasero Derecho'},
      {'id': '26', 'name': 'Compuerta Trasera'},
      {'id': '27', 'name': 'Tapa de motor'},
      {'id': '28', 'name': 'Pescante Derecho'},
      {'id': '29', 'name': 'Pescante Izquierdo'},
      {'id': '30', 'name': 'Cajuela'},
      {'id': '31', 'name': 'Techo'},
      {'id': '32', 'name': 'Tina'},
      {'id': '33', 'name': 'Parrilla'},
      {'id': '34', 'name': 'Otros'}

    ])
    .constant('VEHICLES', [
      {id: 'MICROSUV', url: 'assets/images/microSUV.png'},
      {id: 'TRUCK', url: 'assets/images/camioneta.png'},
      {id: 'JEEP', url: 'assets/images/jeep.png'},
      {id: 'SUV', url: 'assets/images/prado.png'},
      {id: 'SEDAN_HATCHBACK', url: 'assets/images/sedanHatchback.png'},
      {id: 'SEDAN', url: 'assets/images/sedan.png'}
    ]);
})();
