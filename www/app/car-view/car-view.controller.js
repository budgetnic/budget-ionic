(function() {
  'use strict';

  angular
    .module('app.carView')
    .controller('CarViewCtrl', CarViewCtrl);

  /* @ngInject */
  function CarViewCtrl($scope,
                       $state,
                       $stateParams,
                       CarViewService,
                       CarInfoFirebaseService,
                       RevisionService) {

    var vm = $scope;
    vm.selectedOption = undefined;
    vm.goToExteriorParts = goToExteriorParts;
    vm.CarViewService = CarViewService;
    vm.isEditable = (RevisionService.getRevision().type == 'check-out');
    vm.currentCarTraction = CarInfoFirebaseService.carInfo.traction_type;
    activate();

    function activate() {
      vm.selectedOption = $stateParams.option;
      //screen.lockOrientation('portrait');
    }

    function goToExteriorParts() {
      CarViewService.setCanvasComponents();
      $state.go('carParts', { option: vm.selectedOption });
    }
  }
})();
