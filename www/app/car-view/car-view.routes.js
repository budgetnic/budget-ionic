(function() {
  'use strict';

  angular
    .module('app.carView')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('carView', {
        url: '/car_view',
        templateUrl: 'app/car-view/car-view.html',
        controller: 'CarViewCtrl',
        controllerAs: 'vm',
        params: { option: undefined },
        cache: false
      });
  }
})();
