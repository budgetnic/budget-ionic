(function() {
  'use strict';

  angular
    .module('app.carView')
    .factory('CarViewService', CarViewService);

  /* @ngInject */
  function CarViewService(RevisionService) {

    var rootRef = firebase.database().ref();
    var service = {
      damages: [],
      addDamageToCanvasComponents: addDamageToCanvasComponents,
      setCanvasComponents: setCanvasComponents,
      resetDamages: resetDamages,
      pushCarViewData: setDamagesToService,
      rootRef: rootRef,
      damagesLoaded: false
    };
    return service;

    function addDamageToCanvasComponents(damage) {
      service.damages.push(damage);
    }

    function setupDamagesToBePushed() {
      var damagesToPush = angular.copy(service.damages);
      removeIdProperty(damagesToPush);
      return damagesToPush;
    }

    function removeIdProperty(damages) {
      angular.forEach(damages, function(damage) {
        delete damage.shapeId;
      });
    }

    function resetDamages() {
      service.damages = [];
      service.damagesLoaded = false;
    }

    function setCanvasComponents() {
      RevisionService.setDamages(setupDamagesToBePushed());
      RevisionService.setCanvasImage(project.exportSVG({asString: true}));
    }

    function setDamagesToService() {
      if (service.damages.length > 0) {
        RevisionService.setDamages(setupDamagesToBePushed());
        resetDamages();
      }
    }
  }
})();
