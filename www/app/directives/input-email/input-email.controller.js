(function() {
  'use strict';

  angular
    .module('app')
    .controller('InputEmailCtrl', InputEmailCtrl);

  /* @ngInject */
  function InputEmailCtrl($state,
                          $ionicPopup,
                          $scope,
                          SignatureService) {

    var vm = this;
    vm.showEmailInputPopUp = showEmailInputPopUp;

    function showEmailInputPopUp() {
      $scope.data = {};
      $scope.data.language = 'spanish';
      $ionicPopup.show({
        templateUrl: 'app/directives/input-email/templates/template-input-email.html',
        title: 'Ingrese el correo electrónico a quien desea enviar la firma',
        scope: $scope,
        buttons: [
          {text: 'Cancelar'},
          {text: '<b>Aceptar</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.email) {
                e.preventDefault();
              } else {
                SignatureService.setEmail($scope.data.email);
                SignatureService.setLanguage($scope.data.language);
              }
            }
          }
        ]
      });
    }
  }
})();
