(function() {

  angular
    .module('app')
    .directive('inputEmail', inputEmailDirective);

  /* @ngInject */
  function inputEmailDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/input-email/input-email.html',
      controller: 'InputEmailCtrl as vm',
      scope: true
    };

    return directive;
  }

})();
