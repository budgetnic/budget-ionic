(function() {

  angular
    .module('app')
    .directive('inputMva', inputMvaDirective);

  /* @ngInject */
  function inputMvaDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/input-mva/input-mva.html',
      controller: 'InputMVACtrl as vm',
      scope: true
    };

    return directive;
  }

})();
