(function() {
  'use strict';

  angular
    .module('app.login')
    .service('LoginFirebaseService', LoginFirebaseService);

  /* @ngInject */
  function LoginFirebaseService($firebaseObject,
                                SessionService,
                                RevisionService,
                                $rootScope,
                                $location,
                                $state) {

    var rootRef = firebase.database().ref();
    var service = {
      isLoggedIn: isLoggedIn,
      logIn: logIn,
      logOut: logOut,
      verifyAccess: verifyAccess,
      setAuthUser: setAuthUser,
      getAuthUser: getAuthUser
    };

    return service;

    function logIn(username) {
      var reference = rootRef;
      return $firebaseObject(reference.child('users').child(username)).$loaded();
    }

    function userDeliveryPlaceRef(username) {
      var reference = rootRef;
      return $firebaseObject(reference.child('users').child(username).child('delivery_place_ref')).$loaded();
    }

    function isLoggedIn() {
      var authData = SessionService.getAuthData();
      var sessionDefined = typeof authData !== 'undefined';
      var authDataDefined = authData !== null;
      return sessionDefined && authDataDefined;
    }

    function logOut() {
      SessionService.destroy();
      $state.go('login');
    }

    function verifyAccess() {
      if (isLoggedIn()) {
        var username = SessionService.getAuthData();
        RevisionService.setUsername(username);
        userDeliveryPlaceRef(username).then(function(response) {
          RevisionService.setUserDeliveryPlaceRef(response.$value);
          $state.go('MovementsMenu');
        });
      } else {
        $state.go('login');
      }
    }

    function setAuthUser(username) {
      SessionService.setAuthData(username);
    }

    function getAuthUser() {
      return SessionService.getAuthData();
    }

  }

})();
