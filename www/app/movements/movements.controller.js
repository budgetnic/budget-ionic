(function() {
    'use strict';
  
    angular
      .module('app.Movements')
      .controller('MovementsMenuCtrl', MovementsMenuCtrl);
  
    /* @ngInject */
    function MovementsMenuCtrl($state,
                             LoginFirebaseService,
                             $ionicNavBarDelegate) {
  
      var vm = this;
      vm.goToCarRent = goToCarRent;
      vm.goToHomeDelivery = goToHomeDelivery;
      vm.goToCarMov = goToCarMov;
      vm.goToCarOther = goToCarOther;
      vm._authSuccess = _authSuccess;
      vm.goToLogin = goToLogin;

    activate();

    function activate() {
      $ionicNavBarDelegate.showBackButton(false);
    }

      function goToCarRent() {
        $state.go('scannerMenu', { option: "CarRent"});
      }

      function goToHomeDelivery() {
        $state.go('scannerMenu', { option: "HomeDelivery"});
      }

      function goToCarMov() {
        $state.go('scannerMenu', { option: "CarMov"});
      }

      function goToCarOther() {
        $state.go('scannerMenu', { option: "Other"});
      }
    
      function _authSuccess() {
        $state.go('scannerMenu');
      }

      function goToLogin() {
        LoginFirebaseService.logOut();
      }
      
    }

  })();
  