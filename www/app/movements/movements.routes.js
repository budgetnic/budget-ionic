(function() {
    'use strict';
  
    angular
      .module('app.Movements')
      .config(config);
  
    /* @ngInject */
    function config($stateProvider) {
      $stateProvider
        .state('MovementsMenu', {
          url: '/movements',
          templateUrl: 'app/movements/movements.html',
          controller: 'MovementsMenuCtrl',
          controllerAs: 'vm',
          cache: false
        });
    }
  })();
  