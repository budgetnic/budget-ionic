(function() {
  'use strict';

  angular
    .module('app.observations')
    .factory('ObservationsService', ObservationsService);

  /* @ngInject */
  function ObservationsService(RevisionService) {

    var service = {
      observations: [],
      showedObservationModal: false,
      addObservation: addObservation,
      resetObservations: resetObservations,
      getObservations: getObservations,
      setObservationsToService: setObservationsToService,
      removeObservation: removeObservation,
      setShowedObservationModal: setShowedObservationModal,
      getShowedObservationModal: getShowedObservationModal
    };
    return service;

    function removeObservation(observation) {
      var observationIndex = service.observations.indexOf(observation);
      service.observations.splice(observationIndex, 1);
    }

    function getObservations() {
      return service.observations;
    }

    function addObservation(observation) {
      service.observations.push(observation);
    }

    function resetObservations() {
      service.damages = [];
      service.observations = [];
    }

    function setObservationsToService() {
      if (service.observations.length > 0) {
        RevisionService.setObservations(service.observations);
        resetObservations();
      }
    }

    function setShowedObservationModal(value) {
      service.showedObservationModal = value;
    }

    function getShowedObservationModal() {
      return service.showedObservationModal;
    }
  }
})();
