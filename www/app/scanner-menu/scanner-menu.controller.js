(function() {
  'use strict';

  angular
    .module('app.scanMenu')
    .controller('ScannerMenuCtrl', ScannerMenuCtrl);

  /* @ngInject */
  function ScannerMenuCtrl($state,
                           $stateParams,
                           LoginFirebaseService,
                           $ionicNavBarDelegate) {

    var vm = this;
    vm.selectedOption = undefined;
    vm.goToScanner = goToScanner;
    vm.goToLogin = goToLogin;
    vm.goToMovements = goToMovements;

    activate();

    function activate() {
      vm.selectedOption = $stateParams.option;
      $ionicNavBarDelegate.showBackButton(false);
    }

    function goToScanner() {
      $state.go('scanner', { option: vm.selectedOption });
    }

    function goToLogin() {
      LoginFirebaseService.logOut();
    }

    function goToMovements() {
      $state.go('MovementsMenu');
    }

  }
})();
