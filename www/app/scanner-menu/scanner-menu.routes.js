(function() {
  'use strict';

  angular
    .module('app.scanMenu')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('scannerMenu', {
        url: '/scanner_menu',
        templateUrl: 'app/scanner-menu/scanner-menu.html',
        controller: 'ScannerMenuCtrl',
        controllerAs: 'vm',
        params: { option: undefined},
        cache: false
      });
  }
})();
