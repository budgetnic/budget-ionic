(function() {
  'use strict';

  angular
  .module('app.scanner')
  .controller('ScannerCtrl', ScannerCtrl);

  /* @ngInject */
  function ScannerCtrl($state, $stateParams, $cordovaBarcodeScanner, ScannerService) {

    
    $cordovaBarcodeScanner.scan()
      .then(
        function(code) {
          ScannerService.setCode(code.text);
          $state.go('carInfo', { option: $stateParams.option } );
        },
        function(error) {
          alert('Error, no se pudo leer el código');
          $state.go('scanner-error');
        });
  }
})();
