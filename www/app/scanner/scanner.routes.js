(function() {
  'use strict';

  angular
    .module('app.scanner')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('scanner', {
        url: '/scanner',
        templateUrl: 'app/scanner/scanner.html',
        controller: 'ScannerCtrl',
        controllerAs: 'vm',
        params: { option: undefined},        
        cache: false
      });
  }
})();
