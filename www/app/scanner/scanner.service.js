(function() {
  'use strict';

  angular
    .module('app.scanner')
    .service('ScannerService', ScannerService);

  /* @ngInject */
  function ScannerService() {

    var service = {
      code: '',
      setCode: setCode,
      getCode: getCode
    };

    return service;

    function setCode(data) {
      service.code = data.toString();
    }

    function getCode() {
      return service.code;
    }
  }
})();
