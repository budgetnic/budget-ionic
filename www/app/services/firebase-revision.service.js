(function () {
  'use strict';

  angular
    .module('app.services')
    .service('FirebaseRevisionService', FirebaseRevisionService);

  /* @ngInject */
  function FirebaseRevisionService(CarInfoFirebaseService,
    CarViewService,
    RevisionService,
    ObservationsService, $firebaseObject) {

    var rootRef = firebase.database().ref();
    var service = {
      currentRevisionId: null,
      pushNewRevision: pushNewRevision,
      pushDamages: pushDamages,
      pushObservations: pushObservations,
      pushFeedback: pushFeedback,
      getDBRevisionCounter: getDBRevisionCounter,
      updateDBRevisionCounter: updateDBRevisionCounter,
      getRevCounter: getRevCounter,
      setRevCounter: setRevCounter,
      revisionTypeCounter: 0
    };

    return service;

    function pushRevision(newRevision) {
      var revisionRootReference = rootRef
        .child('revisions');
      var pushRef = revisionRootReference.push(newRevision);
      service.currentRevisionId = pushRef.key;
      return pushRef;
    }

    function pushRevisionItems(isCheckIn) {
      if (RevisionService.getObservations()) {
        pushObservations(RevisionService.getObservations());
      }
      if (isCheckIn) {
        pushFeedback(RevisionService.getFeedback());
      }
    }

    function pushNewRevision(newRevision, isCheckIn, timestamp) {
      RevisionService.setTimestamp(timestamp);
      ObservationsService.setObservationsToService();
      var pushRef = pushRevision(newRevision);
      pushRevisionItems(isCheckIn);
      saveCreatedRevisionId(pushRef.key);
    }

    function saveCreatedRevisionId(id) {
      service.currentRevisionId = id;
      updateVehicleCurrentRevisionRef();
    }

    function updateVehicleCurrentRevisionRef() {
      var reference = rootRef
        .child('vehicles')
        .child(CarInfoFirebaseService.carInfo.MVA);
      reference.update({
        last_revision_ref: service.currentRevisionId
      });
    }

    function pushDamagesIdToCurrentRevision(id) {
      var reference = rootRef
        .child('revisions')
        .child(service.currentRevisionId);
      reference.update({
        damages_ref: id
      });
    }

    function pushObservationsIdToCurrentRevision(id) {
      var reference = rootRef
        .child('revisions')
        .child(service.currentRevisionId);
      reference.update({
        observations_ref: id
      });
    }

    function pushDamages(damages) {
      var damagesRootReference = rootRef
        .child('damages');
      var damagesKey = damagesRootReference.push().key;
      var damagesRef = damagesRootReference.child(damagesKey);
      angular.forEach(damages, function (damage) {
        damagesRef.push(damage);
      });
      pushDamagesIdToCurrentRevision(damagesKey);
      CarViewService.resetDamages();
    }

    function pushObservations(observations) {
      var observationsRootReference = rootRef
        .child('observations');
      var observationsKey = observationsRootReference.push().key;
      var observationsRef = observationsRootReference.child(observationsKey);
      angular.forEach(observations, function (observation) {
        observationsRef.push(observation);
      });
      pushObservationsIdToCurrentRevision(observationsKey);
    }

    function pushFeedbackIdToCurrentRevision(id) {
      var reference = rootRef
        .child('revisions')
        .child(service.currentRevisionId);
      reference.update({
        feedback_ref: id
      });
    }

    function pushCurrentRevisionIdToFeedback(id) {
      var reference = rootRef
        .child('feedback')
        .child(id);
      reference.update({
        revision_ref: service.currentRevisionId
      });
    }

    function pushFeedback(feedback) {
      var reference = rootRef
        .child('feedback');
      var pushReference = reference.push(feedback);
      pushFeedbackIdToCurrentRevision(pushReference.key);
      pushCurrentRevisionIdToFeedback(pushReference.key);
    }

    function updateDBRevisionCounter(movementType, value) {
      if (value) {
        var reference = movementType === 'CarMov' ? rootRef.child('revisionTypeCounter').child('transferCounter') : rootRef.child('revisionTypeCounter').child('noneProductiveCounter');
        reference.set(value);

      }
    }

    function getDBRevisionCounter() {
      var reference = rootRef.child('revisionTypeCounter');
      return $firebaseObject(reference).$loaded();
    }

    function getRevCounter() {
      return service.revisionCounter
    }

    function setRevCounter(counter) {
      service.revisionCounter = counter;
    }
  }
})();
