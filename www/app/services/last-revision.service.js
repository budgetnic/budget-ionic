(function() {
  angular
    .module('app.services')
    .factory('LastRevisionService', LastRevisionService);

  /* @ngInject */
  function LastRevisionService(CarInfoFirebaseService,
                               $firebaseObject,
                               $firebaseArray,
                               $q) {

    var rootRef = firebase.database().ref();
    var service =  {
      fetchRevisionData: fetchRevisionData,
      _getLastRevision: _getLastRevision,
      _getLastRevisionDamages: _getLastRevisionDamages,
      _getLastRevisionObservations: _getLastRevisionObservations,
      _getLastRevisionRef: _getLastRevisionRef,
      resetLastRevision: resetLastRevision,
      revision: null
    };
    return service;

    function fetchRevisionData() {
      if (CarInfoFirebaseService.carInfo.MVA) {
        return service._getLastRevision()
          .then(function(data) {
            if (data) {
              service.revision = data;
              return $q.all([service._getLastRevisionDamages(),
                service._getLastRevisionObservations()]);
            }
          });
      }
    }

    function _getLastRevisionRef() {
      var reference = rootRef
        .child('vehicles')
        .child(CarInfoFirebaseService.carInfo.MVA)
        .child('last_revision_ref');
      var lastRevisionRef = $firebaseObject(reference);
      return lastRevisionRef.$loaded()
        .then(function() {
          return lastRevisionRef.$value;
        });
    }

    function _getLastRevision() {
      return service._getLastRevisionRef()
        .then(function(last_revision_ref) {
          if (last_revision_ref) {
            var reference = rootRef
              .child('revisions')
              .child(last_revision_ref);
            var lastRevision = $firebaseObject(reference);
            return lastRevision.$loaded()
              .then(
                function() {
                return lastRevision;
              });
          }
        });
    }

    function _getLastRevisionDamages() {
      var damagesRef = service.revision.damages_ref;
      if (damagesRef) {
        var reference = rootRef
          .child('damages')
          .child(service.revision.damages_ref);
        var lastRevisionDamages = $firebaseArray(reference);
        return lastRevisionDamages.$loaded()
          .then(function() {
          service.revision.damages = lastRevisionDamages;
        });
      }
    }

    function _getLastRevisionObservations() {
      var observationsRef = service.revision.observations_ref;
      if (observationsRef && (service.revision.type === "check-out")) {
        var reference = rootRef
          .child('observations')
          .child(service.revision.observations_ref);
        var lastRevisionObservations = $firebaseArray(reference);
        return lastRevisionObservations.$loaded()
          .then(function() {
            lastRevisionObservations = lastRevisionObservations || [];
            setObservationsIsNewToFalse(lastRevisionObservations);
            service.revision.observations = lastRevisionObservations.map(
              function(observationItem) {
                return {observation: observationItem.observation, is_new: false};
              });
          });
      }
    }

    function setObservationsIsNewToFalse(observations) {
      angular.forEach(observations, function(observation) {
        observation.is_new = false;
      });
    }

    function resetLastRevision() {
      service.revision = null;
    }
  }

})();
