(function () {
  'use strict';

  angular
    .module('app.services')
    .factory('reportSignatureService', reportSignatureService);

  /* @ngInject */
  function reportSignatureService($http,
    $filter,
    SIGNATURE_URL,
    SignatureService,
    RevisionService,
    CarDeliveryInfoService) {

    var service = {
      url: SIGNATURE_URL,
      sendEmail: sendEmail,
      formatRevisionToJson: formatRevisionToJson,
      getNewObservations: getNewObservations,
      formatDamagesToJson: formatDamagesToJson
    };

    return service;

    function sendEmail(email, language, signatureImage) {
      if (!email) {
        email = 'control@budget.com.ni';
      }
      return $http.post(service.url, service.formatRevisionToJson(email,
        language, signatureImage), { 'Content-Type': 'application/json' });
    }

    function formatRevisionToJson(email, language, signatureImage) {
      var dateFilter = $filter('date');
      var damages = RevisionService.getDamages();
      var observations = RevisionService.getObservations();
      var revision = RevisionService.getRevision();

      var json = {
        damages: formatDamagesToJson(damages),
        observations: getNewObservations(observations),
        vehicleType: revision.vehicleType,
        revision: {
          gasLevel: revision.gas_level,
          deliveryPlace: revision.delivery_place,
          km: revision.km,
          timestamp: dateFilter(Date.now(), 'd-MM-yyyy HH:mm:ss'),
          type: revision.type,
          optionType: revision.optionType,
          username: revision.username,
          vehicleMVA: revision.vehicle_ref,
          carParts: revision.car_parts_present,
          canvas: revision.canvas_image,
          contractNumber: revision.contract_number,
          drive: revision.drive,
          phone: revision.phone,
          delivery_place_d: revision.delivery_place_d,
          delivery_place_o: revision.delivery_place_o,
          numberMovement: revision.numberMovement,
          checkOutTime: dateFilter(revision.checkOutTime, 'd-MM-yyyy HH:mm:ss'),
          date: dateFilter(revision.timestamp, 'd-MM-yyyy HH:mm:ss'),
          isTimeExceeded: ((revision.optionType === 'CarMov' || revision.optionType === 'Other') && revision.deliveryTimeInfo) ? revision.deliveryTimeInfo.isTimeExceeded : false,
          deliveryTime: ((revision.optionType === 'CarMov' || revision.optionType === 'Other') && revision.deliveryTimeInfo) ? dateFilter(revision.deliveryTimeInfo.deliveryTime, 'HH:mm:ss') : ''
        },
        deliveryPlaceMail: CarDeliveryInfoService.deliveryPlaceSelected.email,
        ccMail: '',
        email: email,
        language: language,
        signature: signatureImage
      };
      return json;
    }

    function formatDamagesToJson(damages) {
      var formmattedDamages = [];
      damages.forEach(function (damage) {
        formmattedDamages.push({
          damage: damage.damage_type, part: damage.part,
          isNew: damage.is_new, severity: damage.severity_type
        });
      });
      return formmattedDamages;
    }

    function getNewObservations(observations) {
      var newObservations = [];
      if (observations) {
        observations.forEach(function (observation) {
          if (observation.is_new) {
            newObservations.push(observation.observation);
          }
        });
      }
      if (newObservations.length == 0) {
        newObservations.push('Sin observaciones');
      }
      return newObservations;
    }
  }
})();
