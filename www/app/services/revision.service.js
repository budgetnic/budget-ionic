(function () {
  angular
    .module('app.services')
    .factory('RevisionService', RevisionService);

  /* @ngInject */
  function RevisionService() {
    var revision = {};
    var observationsList = [];
    var damagesList = {};
    var feedback = {};
    var service = {
      setUsername: setUsername,
      setUserDeliveryPlaceRef: setUserDeliveryPlaceRef,
      setCarMVA: setCarMVA,
      setNewType: setNewType,
      setTimestamp: setTimestamp,
      setCheckOutTime: setCheckOutTime,
      getTimestamp: getTimestamp,
      getCheckOutTime: getCheckOutTime,
      setCarDeliveryInfo: setCarDeliveryInfo,
      setCarTires: setCarTires,
      setDamages: setDamages,
      addObservation: addObservation,
      setObservations: setObservations,
      removeObservation: removeObservation,
      setCarAccesories: setCarParts,
      setFeedback: setFeedback,
      getFeedback: getFeedback,
      getRevision: getRevision,
      getDamages: getDamages,
      getObservations: getObservations,
      getUserDeliveryPlaceRef: getUserDeliveryPlaceRef,
      setCanvasImage: setCanvasImage,
      setLicensePlate: setLicensePlate,
      setContractNumber: setContractNumber,
      setOptions: setOptions,
      setInfoDriver: setInfoDriver,
      getContractNumber: getContractNumber,
      resetRevision: resetRevision,
      setDeliveryTimeInfo: setDeliveryTimeInfo,
      getDeliveryTimeInfo: getDeliveryTimeInfo,
      setRevId: setRevId,
      getRevId: getRevId
    };
    return service;

    function setCarMVA(MVA) {
      revision.vehicle_ref = MVA;
    }

    function setNewType(lastRevisionType) {
      revision.type = (lastRevisionType === 'check-in') ? 'check-out' : 'check-in'
    }

    function setRevId(revId) {
      revision.numberMovement = revId;
    }

    function getRevId() {
      return revision.numberMovement;
    }

    function setTimestamp(timestamp) {
      revision.timestamp = timestamp;
    }

    function setCheckOutTime(checkOutTime) {
      revision.checkOutTime = checkOutTime;
    }

    function setDeliveryTimeInfo(deliveryTimeInfo) {
      revision.deliveryTimeInfo = deliveryTimeInfo;
    }

    function setUsername(username) {
      revision.username = username;
    }

    function setUserDeliveryPlaceRef(deliveryPlaceRef) {
      revision.deliveryPlaceRef = deliveryPlaceRef;
    }

    function setCarDeliveryInfo(deliveryInfo, optionType) {
      revision.km = deliveryInfo.km;
      revision.delivery_place = deliveryInfo.delivery_place;
      revision.delivery_place_ref = deliveryInfo.delivery_place_ref;
      revision.gas_level = deliveryInfo.gas_level;
      revision.optionType = optionType;
      if (optionType === 'CarMov' || optionType === 'Other') {
        revision.delivery_place_o = deliveryInfo.delivery_place_o;
        revision.delivery_place_o_ref = deliveryInfo.delivery_place_oref;
        revision.delivery_place_d = deliveryInfo.delivery_place_d;
        revision.delivery_place_d_ref = deliveryInfo.delivery_place_dref;
      }
    }

    function setCarTires(tires) {
      revision.tires = tires;
    }

    function setDamages(damages) {
      damagesList = damages;
    }

    function addObservation(observation) {
      observationsList.push(observation);
    }

    function setObservations(observations) {
      observationsList = (observations);
    }

    function removeObservation(observation) {
      var observationIndex = observationsList.indexOf(observation);
      observationsList.splice(observationIndex, 1);
    }

    function setCarParts(carParts, countedCarParts) {
      revision.car_parts_present = carParts;
      revision.car_parts_present.emblems = countedCarParts.emblems;
      revision.car_parts_present.plates = countedCarParts.plates;
    }

    function setFeedback(userFeedback) {
      feedback = userFeedback;
    }

    function getFeedback() {
      return feedback;
    }

    function getRevision() {
      return revision;
    }

    function getDeliveryTimeInfo() {
      return revision.deliveryTimeInfo;
    }

    function getDamages() {
      return damagesList;
    }

    function getObservations() {
      return observationsList;
    }

    function getUserDeliveryPlaceRef() {
      return revision.deliveryPlaceRef;
    }

    function setCanvasImage(canvasImage) {
      revision.canvas_image = canvasImage;
    }

    function setLicensePlate(licensePlate) {
      revision.license_plate = licensePlate;
    }

    function getTimestamp() {
      return revision.timestamp;
    }

    function getCheckOutTime() {
      return revision.checkOutTime;
    }


    function setOptions(option) {
      switch (option) {
        case "CarMov":
          revision.option = "Traslado del vehículo";
          revision.optionType = "CarMov";
          break;
        case "CarRent":
          revision.option = "Alquiler del vehículo";
          revision.optionType = "CarRent";
          break;
        case "HomeDelivery":
          revision.option = "Entrega a domicilio";
          revision.optionType = "HomeDelivery";
          break;
        case "Other":
          revision.option = "Uso no productivo";
          revision.optionType = "Other";
          break;
        default:
          revision.option = "";
          revision.optionType = "";
      }
    }

    function setInfoDriver(drive, phone, numberMovement) {
      revision.drive = drive;
      revision.phone = phone;
      revision.numberMovement = numberMovement;
    }

    function setContractNumber(contractNumber) {
      if (!contractNumber) {
        contractNumber = '';
      }
      revision.contract_number = contractNumber;
    }

    function getContractNumber() {
      return revision.contract_number;
    }

    function resetRevision() {
      revision = {};
      damagesList = {};
      observationsList = [];
    }
  }
})();