(function () {
  'use strict';
  angular
    .module('app.signature')
    .constant('LANGUAGES', [
      { key: 'spanish', value: 'Español' },
      { key: 'english', value: 'Inglés' }
    ])
    .constant('DEFAULT_LANGUAGE', { key: 'spanish', value: 'Español' })
    .constant('DELIVERY_DISTANCES', [
      [null, 16, 15, 15, 142, 50, 126, 146, 15, 15, 15],
      [16, null, 7, 3, 137, 50, 139, 144, 3, 3, 7],
      [15, 7, null, 6, 146, 46, 137, 149, 6, 6, 7],
      [15, 3, 6, null, 142, 49, 142, 145, null, null, 6],
      [142, 137, 146, 142, null, 191, 169, 282, 142, 142, 146],
      [50, 50, 46, 49, 191, null, 157, 105, 49, 49, 46],
      [126, 139, 137, 142, 169, 157, null, 248, 137, 137, 137],
      [146, 144, 149, 145, 282, 105, 248, null, 145, 145, 149],
      [15, 3, 6, null, 142, 49, 137, 145, null, null, 6],
      [15, 3, 6, null, 142, 49, 137, 145, null, null, 6],
      [15, 7, 7, 6, 146, 46, 137, 149, 6, 6, null]
    ])
    .constant('DELIVERY_TIMES', [
      [null, 2100, 1800, 1800, 10200, 3600, 7200, 9000, 1800, 1800, 1800],
      [2100, null, 600, 300, 8100, 3600, 9000, 9600, 300, 300, 600],
      [1800, 600, null, 600, 8400, 3000, 8400, 9000, 600, 600, 1200],
      [1800, 300, 600, null, 8400, 3600, 9000, 9600, null, null, 600],
      [10200, 8100, 8400, 8400, null, 10800, 9600, 16800, 8400, 8400, 8400],
      [3600, 3600, 3000, 3600, 10800, null, 9300, 6000, 3600, 3600, 3000],
      [7200, 9300, 8400, 9000, 9600, 9300, null, 14400, 9000, 9000, 8400],
      [9000, 9600, 9000, 9600, 16800, 6000, 14400, null, 9600, 9600, 9000],
      [1800, 300, 600, null, 8400, 3600, 9000, 9600, null, null, 600],
      [1800, 300, 600, null, 8400, 3600, 9000, 9600, null, null, 600],
      [1800, 600, 1200, 600, 8400, 3000, 8400, 9000, 600, 600, null]
    ])
    .constant('DELIVERY_PLACES', [
      { name: 'aeropuerto', position: 0 },
      { name: 'holiday_inn', position: 1 },
      { name: 'carretera_masaya km.12', position: 2 },
      { name: 'oficina_central', position: 3 },
      { name: 'chinandega', position: 4 },
      { name: 'granada', position: 5 },
      { name: 'matagalpa', position: 6 },
      { name: 'san_juan', position: 7 },
      { name: 'flota', position: 8 },
      { name: 'taller', position: 9 },
      { name: 'oficina_villa fontana', position: 10 }
    ]);
})();
