(function () {
  'use strict';

  angular
    .module('app.signature')
    .controller('SignatureCtrl', SignatureCtrl);

  /* @ngInject */
  function SignatureCtrl($state,
    $scope,
    $filter,
    $stateParams,
    $ionicNavBarDelegate,
    $ionicPopup,
    $ionicLoading,
    SignatureService,
    lodash,
    reportSignatureService,
    LastRevisionService,
    RevisionService,
    CarDeliveryInfoService,
    FirebaseRevisionService,
    ObservationsService,
    LANGUAGES,
    DEFAULT_LANGUAGE,
    DELIVERY_PLACES,
    DELIVERY_TIMES) {

    var vm = this;
    vm.selectedOption = undefined;
    vm.disabledEmail = ($stateParams.option === 'CarMov' || $stateParams.option === 'Other') ? "si":"no"
    var canvas = document.getElementById('signatureCanvas');
    var signaturePad = new SignaturePad(canvas);
    vm.clearCanvas = clearCanvas;
    vm.saveCanvas = saveCanvas;
    vm.saveAvailable = false;
    vm.languages = LANGUAGES;
    vm.RevisionService = RevisionService;
    var lastRevision = LastRevisionService.revision;
    vm.revision = RevisionService.getRevision();
    var checkOutTime = (vm.revision.type === 'check-out' || !lastRevision) ? 0 : lastRevision.timestamp;
    RevisionService.setCheckOutTime(checkOutTime);
    RevisionService.setTimestamp(new Date());
    activate();

    function activate() {
      vm.selectedOption = $stateParams.option;
      var deliveryTimeInfo = (vm.revision.type === 'check-in' && (vm.selectedOption === 'CarMov' || vm.selectedOption === 'Other')) ? getDeliveryDurationInfo() : null;
      RevisionService.setDeliveryTimeInfo(deliveryTimeInfo);
      document.getElementById('deliveryTime').className = (vm.revision.type === 'check-in' && vm.selectedOption === 'CarMov' && deliveryTimeInfo.isTimeExceeded) ? 'isTimeExceededLabel' : 'item-note';
      vm.contract_number = RevisionService.getContractNumber();
      vm.hasPreviousRevision = (typeof vm.contract_number != 'undefined');
      $ionicNavBarDelegate.showBackButton(false);
      setDefaultLanguage();
      resizeCanvas();
    }

    function clearCanvas() {
      signaturePad.clear();
    }

    function updateRevisionCounterByMovementType(movementType) {
      var revCounter = FirebaseRevisionService.getRevCounter();
      FirebaseRevisionService.updateDBRevisionCounter(movementType, revCounter);
    }

    function resizeCanvas() {
      var ratio = Math.max(window.devicePixelRatio || 1, 1);
      canvas.width = canvas.offsetWidth * ratio;
      canvas.height = canvas.offsetHeight * ratio;
      canvas.getContext('2d').scale(ratio, ratio);
    }

    async function saveCanvas() {
      canvas = document.getElementById('signatureCanvas');
      const context = canvas.getContext('2d');
      
      const pixelBuffer = new Uint32Array(
        context.getImageData(0, 0, canvas.width, canvas.height).data.buffer
      );
      var isCanvasEMpty = !pixelBuffer.some(color => color !== 0);
      if (isCanvasEMpty) {
        $ionicPopup.show({
          template: '<p>Espacio de firma en blanco, por favor agregue su firma<p/>',
          title: 'Reporte no generado',
          scope: $scope,
          buttons: [
            {text: 'Aceptar'}
          ]
        });
      } else {
        RevisionService.setContractNumber(vm.contract_number);
        var signatureImage = signaturePad.toDataURL();
        $ionicLoading.show({
          template: 'Procesando reporte...'
        });
        reportSignatureService.sendEmail(vm.email, vm.language.key, signatureImage)
          .then(delay(15000))
          .then(handleSendEmailSuccess)
          .catch(catchSendEmailError);
      }
    }

    function handleSendEmailSuccess() {
      pushAndEndProcess();
      LastRevisionService.resetLastRevision();
      RevisionService.resetRevision();
      SignatureService.resetSignature();
      CarDeliveryInfoService.deliveryPlaceSelected = {};
      $ionicLoading.hide();
      $state.go('login');
    }

    function catchSendEmailError() {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Error de conexión',
        template: 'Por favor intente más tarde.'
      });
    }

    function setDefaultLanguage() {
      vm.language = DEFAULT_LANGUAGE;
    }

    function pushAndEndProcess() {
      FirebaseRevisionService.pushNewRevision(RevisionService.getRevision(),
        false, RevisionService.getTimestamp().getTime());
      FirebaseRevisionService.pushDamages(RevisionService.getDamages());
      if (vm.revision.type === 'check-out')
        updateRevisionCounterByMovementType(RevisionService.getRevision().optionType);
      ObservationsService.setShowedObservationModal(false);
    }

    function delay(t) {
      return new Promise(function (resolve) {
        setTimeout(resolve, t);
      });
    }

    function getDeliveryPlacePosition(deliveryPlaceName, deliveryPlaces) {
      return lodash.find(deliveryPlaces,function (place){
        return place.name === deliveryPlaceName.toLowerCase().replace(' ', '_');
      }).position;
    }

    function getInfoFromMatrix(row, col, information) {
      return information[row][col];
    }

    function getDeliveryDurationInfo() {
      var deliveryTimestamp = new Date();
      deliveryTimestamp.setHours(0, 0, 0, 0);
      var originIndex = getDeliveryPlacePosition(vm.revision.delivery_place_o, DELIVERY_PLACES);
      var destinyIndex = getDeliveryPlacePosition(vm.revision.delivery_place_d, DELIVERY_PLACES);
      var approxDeliveryTime = getInfoFromMatrix(originIndex, destinyIndex, DELIVERY_TIMES);
      var realDeliveryTime = Math.abs(vm.revision.timestamp.getTime() - checkOutTime) / 1000;
      deliveryTimestamp.setSeconds(realDeliveryTime);
      return { isTimeExceeded: (realDeliveryTime > approxDeliveryTime), deliveryTime: deliveryTimestamp.getTime() };
    }
  }

})();