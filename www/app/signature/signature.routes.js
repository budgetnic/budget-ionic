(function() {
  'use strict';

  angular
    .module('app.signature')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('signature', {
        url: '/signature',
        templateUrl: 'app/signature/signature.html',
        controller: 'SignatureCtrl',
        controllerAs: 'vm',
        params: { option: undefined },
        cache: false
      });
  }

})();
