(function() {
  'use strict';

  angular
    .module('app.signature')
    .service('SignatureService', SignatureService);

  /* @ngInject */
  function SignatureService() {

    var signatureService = {};
    var service = {
      setEmail: setEmail,
      setLanguage: setLanguage,
      setSignature: setSignature,
      getEmail: getEmail,
      getLanguage: getLanguage,
      getSignature: getSignature,
      resetSignature: resetSignature
    };

    return service;

    function setEmail(email) {
      signatureService.email = email;
    }

    function setLanguage(language) {
      signatureService.language = language;
    }

    function setSignature(signature) {
      signatureService.signature = signature;
    }

    function getEmail() {
      return signatureService.email;
    }

    function getLanguage() {
      return signatureService.language;
    }

    function getSignature() {
      return signatureService.signature;
    }

    function resetSignature() {
      signatureService.email = undefined;
      signatureService.signature = undefined;
      signatureService.language = 'spanish';
    }
  }

})();
