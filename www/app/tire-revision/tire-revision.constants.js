(function() {
'use strict';

angular
    .module('app.tireRevision')
    .constant('SELECTED_TIRES', [
      {rightFrontTireSelectedOption: 'Bridgestone'},
      {leftFrontTireSelectedOption: 'Bridgestone'},
      {leftBackTireSelectedOption: 'Bridgestone'},
      {rightBackTireSelectedOption: 'Bridgestone'},
      {extraTireSelectedOption: 'Bridgestone'}
    ])
    .constant('TIRE_BRANDS', [
      'Dunlop',
      'Bridgestone',
      'Yokohama',
      'Firestone',
      'Pirelli',
      'Kumho',
      'Hankook',
      'Goodyear',
      'Michelin',
      'Toyo',
      'Maxxis',
      'Otros'
    ])
    .constant('VEHICLES_UPPER_VIEW', [
      {id: 'MICROSUV', url: 'assets/images/microSUVCar.png'},
      {id: 'TRUCK', url: 'assets/images/camionetaCar.png'},
      {id: 'JEEP', url: 'assets/images/jeepCar.png'},
      {id: 'SUV', url: 'assets/images/pradoCar.png'},
      {id: 'SEDAN_HATCHBACK', url: 'assets/images/sedanHatchbackCar.png'},
      {id: 'SEDAN', url: 'assets/images/sedanCar.png'}
    ]);
})();
