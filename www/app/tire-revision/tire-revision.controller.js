(function() {
  'use.strict';

  angular
  .module('app.tireRevision')
  .controller('TireRevisionCtrl', TireRevisionCtrl);

  /* @ngInject */
  function TireRevisionCtrl(SELECTED_TIRES,
                            TIRE_BRANDS,
                            $state,
                            $stateParams,
                            VEHICLES_UPPER_VIEW,
                            CarInfoFirebaseService,
                            LastRevisionService,
                            RevisionService) {

    var vm = this;
    vm.selectedOption = undefined;
    vm.goToCarView = goToCarView;
    vm.tireBrands = TIRE_BRANDS;
    vm.selectedTires = SELECTED_TIRES;

    activate();

    function activate() {
      vm.selectedOption = $stateParams.option;
      var vehicleType = CarInfoFirebaseService.carInfo.type;
      (LastRevisionService.revision) ? setPreviousTires() : setDefaultTires();

      for (var vehicleIndex in VEHICLES_UPPER_VIEW) {
        if (VEHICLES_UPPER_VIEW[vehicleIndex].id == vehicleType) {
          vm.currentCarType = VEHICLES_UPPER_VIEW[vehicleIndex].url;
          break;
        }
      }
    }

    function setDefaultTires() {
      vm.selectedTires.rightFrontTireSelectedOption = SELECTED_TIRES[0].rightFrontTireSelectedOption;
      vm.selectedTires.leftFrontTireSelectedOption = SELECTED_TIRES[1].leftFrontTireSelectedOption;
      vm.selectedTires.leftBackTireSelectedOption = SELECTED_TIRES[2].leftBackTireSelectedOption;
      vm.selectedTires.rightBackTireSelectedOption = SELECTED_TIRES[3].rightBackTireSelectedOption;
      vm.selectedTires.extraTireSelectedOption = SELECTED_TIRES[4].extraTireSelectedOption;
    }

    function setPreviousTires() {
      try {
        vm.selectedTires.rightFrontTireSelectedOption =
         LastRevisionService.revision.tires.right_front;
        vm.selectedTires.leftFrontTireSelectedOption =
         LastRevisionService.revision.tires.left_front;
        vm.selectedTires.leftBackTireSelectedOption =
         LastRevisionService.revision.tires.left_rear;
        vm.selectedTires.rightBackTireSelectedOption =
         LastRevisionService.revision.tires.right_rear;
        vm.selectedTires.extraTireSelectedOption =
         LastRevisionService.revision.tires.spare;
      } catch (err) {
        setDefaultTires();
      }
    }

    function goToCarView() {
      RevisionService.setCarTires({
        right_front: vm.selectedTires.rightFrontTireSelectedOption,
        left_front: vm.selectedTires.leftFrontTireSelectedOption,
        right_rear: vm.selectedTires.rightBackTireSelectedOption,
        left_rear: vm.selectedTires.leftBackTireSelectedOption,
        spare: vm.selectedTires.extraTireSelectedOption
      });
      $state.go('carView', { option: vm.selectedOption });
    }
  }
})();
