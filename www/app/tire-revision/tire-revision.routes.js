(function() {
  'use strict';

  angular
    .module('app.tireRevision')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('tireRevision', {
        url: '/tire_revision',
        templateUrl: 'app/tire-revision/tire-revision.html',
        controller: 'TireRevisionCtrl',
        controllerAs: 'vm',
        params: { option: undefined },
        cache: false,
        resolve: {
          'CarInfoService': function(CarInfoFirebaseService, LastRevisionService) {
            return CarInfoFirebaseService.fetchCarInfo()
              .then(
                function() {
                  return LastRevisionService.fetchRevisionData();
                });
          }
        }
      });
  }
})();
